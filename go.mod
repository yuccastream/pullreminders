module gitlab.com/yuccastream/pullreminders

go 1.14

require (
	github.com/Syfaro/telegram-bot-api v4.6.4+incompatible
	github.com/dustin/go-humanize v1.0.0
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible // indirect
	github.com/hashicorp/go-retryablehttp v0.6.6
	github.com/robfig/cron v1.2.0
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.6.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/xanzy/go-gitlab v0.32.0
	gopkg.in/yaml.v2 v2.3.0
)
