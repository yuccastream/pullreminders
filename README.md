# Pull Reminders

Напоминание о проведении ревью открытых Merge Request в GitLab.

## Конфигурация

```yaml
---
method: approvals # or labels
ptal: true # please take a look; works with method=approvals
approvals: all # or direct, or suggested
can_be_merged: true # include MRs that can be merged
group_ids: [0]
# project_ids: [0]
access_token: GITLAB_TOKEN
schedule: "0 0 7 * * 0,6"
review_label_prefix: "review-needed/" # works with method=labels
# dry_run: true # runs without notification
# base_url: "https://gitlab.com/" 
# empty_message: "Nothing to review."
# excluded_authors: ["renovatebot"]
# default_reviewers: ["user1", "user2"]
wip: false
labels_match_re:
  - size/.*
  - kind/.*
  - priority/.*
telegram_config:
  enabled: true
  chat_id: -1
  token: TELEGRAM_TOKEN
slack_config:
  enabled: true
  api_url: SLACK_API_URL
  channel: "#prs"
  # icon_emoji: ""
  # icon_url: ""
  # link_names: false
```

Чувствительные данные могут быть заданы через переменные окружения:

* `ACCESS_TOKEN` – GitLab Access Token (параметр `access_token`);
* `SLACK_API_URL` – Slack API URL (параметр `slack_config.api_url`);
* `TELEGRAM_TOKEN` – Telegram Bot Token (параметр `telegram_config.token`).

Переменные окружения для настройки логирования:

* `LOG_LEVEL` – уровень логирования;
* `LOG_FORMAT` – формат сообщений (`text` или `json`).

## Запуск

```shell
Usage of ./pullreminders:
  -config string
    	Configuration file in YAML (default "/etc/pullreminders/config.yaml")
  -lint
    	Lint configuration and exit
  -one-shot
    	Runs without scheduler
```

## Установка в кубернетес

```shell
helm upgrade -install pullreminder chart
```
