package main

import (
	"io/ioutil"
	"os"
	"regexp"
	"strings"

	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v2"
)

const (
	defaultBaseURL = "https://gitlab.com/"
	defaultMethod  = "labels"
)

type Config struct {
	AccessToken       string           `yaml:"access_token"`
	Approvals         string           `yaml:"approvals"`
	BaseURL           string           `yaml:"base_url"`
	CanBeMerged       bool             `yaml:"can_be_merged"`
	DefaultReviewers  []string         `yaml:"default_reviewers"`
	DryRun            bool             `yaml:"dry_run"`
	EmptyMessage      string           `yaml:"empty_message"`
	ExcludedAuthors   []string         `yaml:"excluded_authors"`
	GroupIDs          []int            `yaml:"group_ids"`
	LabelsMatch       []string         `yaml:"labels_match_re"`
	Method            string           `yaml:"method"`
	PTAL              bool             `yaml:"ptal"`
	ProjectIDs        []int            `yaml:"project_ids"`
	ReviewLabelPrefix string           `yaml:"review_label_prefix"`
	Schedule          string           `yaml:"schedule"`
	SlackConfig       SlackConfig      `yaml:"slack_config"`
	TelegramConfig    TelegramConfig   `yaml:"telegram_config"`
	WIP               bool             `yaml:"wip"`
	labelsMatchRe     []*regexp.Regexp `yaml:"-"`
}

type TelegramConfig struct {
	ChatID  int64  `yaml:"chat_id"`
	Enabled bool   `yaml:"enabled"`
	Token   string `yaml:"token"`
}

type SlackConfig struct {
	APIURL    string `yaml:"api_url"`
	Channel   string `yaml:"channel"`
	Enabled   bool   `yaml:"enabled"`
	IconEmoji string `yaml:"icon_emoji"`
	IconURL   string `yaml:"icon_url"`
	LinkNames *bool  `yaml:"link_names"`
}

func LoadConfig(confFile string) (*Config, error) {
	data, err := ioutil.ReadFile(confFile)
	if err != nil {
		return nil, err
	}
	config := &Config{}
	if err := yaml.Unmarshal(data, &config); err != nil {
		return nil, err
	}
	config.FillDefaults()
	for _, label := range config.LabelsMatch {
		re, err := regexp.Compile(label)
		if err != nil {
			return nil, err
		}
		config.labelsMatchRe = append(config.labelsMatchRe, re)
	}
	config.Approvals = strings.ToLower(config.Approvals)
	return config, nil
}

func (c *Config) LoadSensitiveDataFromEnv() {
	if at := os.Getenv("ACCESS_TOKEN"); len(at) != 0 {
		c.AccessToken = at
	}
	if tt := os.Getenv("TELEGRAM_TOKEN"); len(tt) != 0 {
		c.TelegramConfig.Token = tt
	}
	if au := os.Getenv("SLACK_API_URL"); len(au) != 0 {
		c.SlackConfig.APIURL = au
	}
}

func (c *Config) FillDefaults() {
	// По умолчанию линковка имен включена
	if c.SlackConfig.LinkNames == nil {
		c.SlackConfig.LinkNames = gitlab.Bool(true)
	}
	if len(c.BaseURL) == 0 {
		c.BaseURL = defaultBaseURL
	}
	if len(c.Method) == 0 {
		c.Method = defaultMethod
	}
}
