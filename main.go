package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

var (
	configFile = flag.String("config", "/etc/pullreminders/config.yaml", "Configuration file in YAML")
	lintConfig = flag.Bool("lint", false, "Lint configuration and exit")
	oneShot    = flag.Bool("one-shot", false, "Runs without scheduler")
)

func main() {
	flag.Parse()

	logger := NewLogger(os.Stdout, "pullreminders")

	config, err := LoadConfig(*configFile)
	if err != nil {
		logger.Fatal(err)
	}

	config.LoadSensitiveDataFromEnv()
	pr, err := NewPullReminders(logger, config)
	if err != nil {
		logger.Fatal(err)
	}

	if *lintConfig {
		fmt.Println("Looks good")
		return
	}

	if *oneShot {
		pr.OneShot()
		return
	}

	logger.Println("Starting pullreminders...")
	pr.Start()

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	s := <-signalChan

	logger.Printf("Captured %v. Exiting...", s)
	pr.Stop()
}
