package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
)

type MessageItem struct {
	ProjectID            int
	ProjectName          string
	ProjectURL           string
	MergeRequestCreateAt time.Time
	MergeRequestID       int
	MergeRequestTitle    string
	MergeRequestURL      string
	Labels               []string
	Reviewers            []string
	Author               string
	CanBeMerged          bool
}

func (m *MessageItem) AsHTML() string {
	var labelsText string
	if len(m.Labels) > 0 {
		labelsText = fmt.Sprintf(" (%s)", strings.Join(m.Labels, ", "))
	}
	mergeStatus := "Can be merged"
	if len(m.Reviewers) > 0 {
		mergeStatus = fmt.Sprintf("Waiting on %s", strings.Join(m.Reviewers, ", "))
	} else if len(m.Author) != 0 {
		mergeStatus = fmt.Sprintf("%s by %s", mergeStatus, m.Author)
	}
	return fmt.Sprintf(
		"[<a href=\"%s\">%s</a>] <a href=\"%s\">%s</a>%s\n%s · %s",
		m.ProjectURL,
		m.ProjectName,
		m.MergeRequestURL,
		m.MergeRequestTitle,
		labelsText,
		humanize.Time(m.MergeRequestCreateAt),
		mergeStatus,
	)
}

func (m *MessageItem) AsMrkdwn() string {
	var labelsText string
	if len(m.Labels) > 0 {
		labelsText = fmt.Sprintf(" (%s)", strings.Join(m.Labels, ", "))
	}
	mergeStatus := "Can be merged"
	if len(m.Reviewers) > 0 {
		mergeStatus = fmt.Sprintf("Waiting on %s", strings.Join(m.Reviewers, ", "))
	} else if len(m.Author) != 0 {
		mergeStatus = fmt.Sprintf("%s by %s", mergeStatus, m.Author)
	}
	return fmt.Sprintf(
		"[<%s|%s>] <%s|%s>%s\n%s · %s",
		m.ProjectURL,
		m.ProjectName,
		m.MergeRequestURL,
		m.MergeRequestTitle,
		labelsText,
		humanize.Time(m.MergeRequestCreateAt),
		mergeStatus,
	)
}
