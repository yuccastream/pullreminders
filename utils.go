package main

import (
	"io"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
)

func NewLogger(out io.Writer, component string) *logrus.Entry {
	logrus.SetOutput(out)
	if logLevel := os.Getenv("LOG_LEVEL"); len(logLevel) > 0 {
		level, err := logrus.ParseLevel(logLevel)
		if err == nil {
			logrus.SetLevel(level)
		}
	}
	switch strings.ToLower(os.Getenv("LOG_FORMAT")) {
	case "text":
		logrus.SetFormatter(&logrus.TextFormatter{})
	default:
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}
	return logrus.WithField("component", component)
}

func Unique(strSlice []string) []string {
	list := []string{}
	keys := make(map[string]struct{})
	for _, entry := range strSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = struct{}{}
			list = append(list, entry)
		}
	}
	return list
}
