package main

import (
	"github.com/xanzy/go-gitlab"
)

func ReviewerNamesSuggestedByApprovals(approvals *gitlab.MergeRequestApprovals) []string {
	var (
		reviewers  []string
		approvedBy []*gitlab.MergeRequestApproverUser
	)
	if approvals.SuggestedApprovers == nil {
		return []string{}
	}
	if approvals.ApprovedBy != nil {
		approvedBy = approvals.ApprovedBy
	}
	for _, approver := range approvals.SuggestedApprovers {
		hit := false
		for _, appBy := range approvedBy {
			if approver.Username == appBy.User.Username {
				hit = true
				break
			}
		}
		if !hit {
			reviewers = append(reviewers, approver.Username)
		}
	}
	return Unique(reviewers)
}

func ReviewerNamesDirectByApprovals(approvals *gitlab.MergeRequestApprovals) []string {
	var (
		reviewers  []string
		approvedBy []*gitlab.MergeRequestApproverUser
	)
	if approvals.Approvers == nil {
		return []string{}
	}
	if approvals.ApprovedBy != nil {
		approvedBy = approvals.ApprovedBy
	}
	for _, approver := range approvals.Approvers {
		hit := false
		for _, appBy := range approvedBy {
			if approver.User.Username == appBy.User.Username {
				hit = true
				break
			}
		}
		if !hit {
			reviewers = append(reviewers, approver.User.Username)
		}
	}
	return Unique(reviewers)
}

func ReviewerNamesByApprovals(approvals *gitlab.MergeRequestApprovals) []string {
	reviewersDirect := ReviewerNamesDirectByApprovals(approvals)
	reviewersEligible := ReviewerNamesSuggestedByApprovals(approvals)
	reviewers := append(reviewersDirect, reviewersEligible...)
	return Unique(reviewers)
}
