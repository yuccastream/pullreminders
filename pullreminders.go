package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	tgbotapi "github.com/Syfaro/telegram-bot-api"
	"github.com/robfig/cron"
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

const (
	defaultPTALText = "🦸"
)

var (
	httpClient = &http.Client{
		Timeout:   time.Second * 10,
		Transport: RetryTransport(),
	}
)

type PullReminders struct {
	logger      *logrus.Entry
	config      *Config
	cronCli     *cron.Cron
	gitlabCli   *gitlab.Client
	telegramCli *tgbotapi.BotAPI
}

func NewPullReminders(logger *logrus.Entry, config *Config) (*PullReminders, error) {
	c := cron.New()

	if logger == nil {
		logger = logrus.WithField("component", "pullreminders")
	}

	p := &PullReminders{
		logger:  logger,
		config:  config,
		cronCli: c,
	}

	client, err := gitlab.NewClient(config.AccessToken,
		gitlab.WithHTTPClient(httpClient),
		gitlab.WithBaseURL(config.BaseURL),
	)
	if err != nil {
		return nil, err
	}
	p.gitlabCli = client

	err = p.cronCli.AddFunc(config.Schedule, p.run)
	if err != nil {
		return nil, err
	}

	err = p.setupNotifiers()
	if err != nil {
		return nil, err
	}

	return p, nil
}

func (p *PullReminders) setupNotifiers() error {
	if p.config.TelegramConfig.Enabled {
		tgbot, err := tgbotapi.NewBotAPI(p.config.TelegramConfig.Token)
		if err != nil {
			return err
		}
		p.telegramCli = tgbot
	}
	return nil
}

func (p *PullReminders) OneShot() {
	p.run()
}

func (p *PullReminders) Start() {
	p.cronCli.Start()
	p.printsCronNext()
}

func (p *PullReminders) Stop() {
	p.cronCli.Stop()
}

func (p *PullReminders) IsExcluded(mr *gitlab.MergeRequest) bool {
	if len(p.config.ExcludedAuthors) == 0 {
		return false
	}
	for _, author := range p.config.ExcludedAuthors {
		if mr.Author.Username == author {
			return true
		}
	}
	return false
}

func (p *PullReminders) getProjectMergeRequests(pid int) ([]*gitlab.MergeRequest, error) {
	page := 1
	var mergeRequests []*gitlab.MergeRequest
	for {
		options := &gitlab.ListProjectMergeRequestsOptions{
			ListOptions: gitlab.ListOptions{
				Page: page,
			},
			State: gitlab.String("opened"),
		}
		mrs, r, err := p.gitlabCli.MergeRequests.ListProjectMergeRequests(pid, options, nil)
		if err != nil {
			return nil, err
		}
		for _, mr := range mrs {
			if yes := p.IsExcluded(mr); yes {
				continue
			}
			mergeRequests = append(mergeRequests, mr)
		}
		nextPageRaw := r.Header.Get("X-Next-Page")
		if len(nextPageRaw) == 0 {
			break
		}
		nextPage, err := strconv.Atoi(nextPageRaw)
		if err != nil {
			break
		}
		page = nextPage
	}
	return mergeRequests, nil
}

func (p *PullReminders) getGroupMergeRequests(gid int) ([]*gitlab.MergeRequest, error) {
	page := 1
	var mergeRequests []*gitlab.MergeRequest
	for {
		options := &gitlab.ListGroupMergeRequestsOptions{
			ListOptions: gitlab.ListOptions{
				Page: page,
			},
			State: gitlab.String("opened"),
		}
		mrs, r, err := p.gitlabCli.MergeRequests.ListGroupMergeRequests(gid, options, nil)
		if err != nil {
			return nil, err
		}
		for _, mr := range mrs {
			if yes := p.IsExcluded(mr); yes {
				continue
			}
			mergeRequests = append(mergeRequests, mr)
		}
		nextPageRaw := r.Header.Get("X-Next-Page")
		if len(nextPageRaw) == 0 {
			break
		}
		nextPage, err := strconv.Atoi(nextPageRaw)
		if err != nil {
			break
		}
		page = nextPage
	}
	return mergeRequests, nil
}

func (p *PullReminders) getGroupProjects(gid int) (map[int]*gitlab.Project, error) {
	page := 1
	projects := map[int]*gitlab.Project{}
	for {
		options := &gitlab.ListGroupProjectsOptions{
			ListOptions: gitlab.ListOptions{
				Page: page,
			},
			OrderBy:          gitlab.String("updated_at"),
			Archived:         gitlab.Bool(false),
			IncludeSubgroups: gitlab.Bool(true),
		}
		fetchedProjects, r, err := p.gitlabCli.Groups.ListGroupProjects(gid, options, nil)
		if err != nil {
			return nil, err
		}
		for _, project := range fetchedProjects {
			projects[project.ID] = project
		}
		nextPageRaw := r.Header.Get("X-Next-Page")
		if len(nextPageRaw) == 0 {
			break
		}
		nextPage, err := strconv.Atoi(nextPageRaw)
		if err != nil {
			break
		}
		page = nextPage
	}
	return projects, nil
}

func (p *PullReminders) filterLabels(labels []string) []string {
	var labelsResult []string
	for _, label := range labels {
		for _, labelRE := range p.config.labelsMatchRe {
			if labelRE.MatchString(label) {
				labelsResult = append(labelsResult, label)
				break
			}
		}
	}
	return labelsResult
}

func (p *PullReminders) reviewersNeededByLabels(mr *gitlab.MergeRequest) []string {
	var reviewers []string
	labels := mr.Labels
	for _, label := range labels {
		if strings.HasPrefix(label, p.config.ReviewLabelPrefix) {
			reviewer := strings.TrimPrefix(label, p.config.ReviewLabelPrefix)
			reviewers = append(reviewers, fmt.Sprintf("@%s", reviewer))
		}
	}
	return reviewers
}

func (p *PullReminders) reviewersNeededByApprovals(mr *gitlab.MergeRequest) []string {
	approvals, _, err := p.gitlabCli.MergeRequests.GetMergeRequestApprovals(mr.ProjectID, mr.IID, nil)
	if err != nil {
		return []string{}
	}

	var reviewers []string
	switch p.config.Approvals {
	case "direct":
		reviewers = ReviewerNamesDirectByApprovals(approvals)
	case "suggested":
		reviewers = ReviewerNamesSuggestedByApprovals(approvals)
	default:
		reviewers = ReviewerNamesByApprovals(approvals)
	}

	if len(reviewers) == 0 && len(p.config.DefaultReviewers) > 0 {
		reviewers = p.config.DefaultReviewers
	}

	var reviewersResult []string
	for _, approver := range reviewers {
		if mr.Author.Username == approver {
			continue
		}
		reviewersResult = append(reviewersResult, fmt.Sprintf("@%s", approver))
	}

	if len(reviewersResult) == 0 && p.config.PTAL {
		return []string{defaultPTALText}
	}

	return reviewersResult
}

func (p *PullReminders) getMergeRequestReviewers(mr *gitlab.MergeRequest) []string {
	switch p.config.Method {
	case "labels":
		return p.reviewersNeededByLabels(mr)
	case "approvals":
		return p.reviewersNeededByApprovals(mr)
	default:
		return []string{}
	}
}

func (p *PullReminders) projectProject(pid int) []MessageItem {
	log := p.logger.WithField("project_id", strconv.Itoa(pid))

	project, _, err := p.gitlabCli.Projects.GetProject(pid, &gitlab.GetProjectOptions{})
	if err != nil {
		log.WithError(err).Error("Failed to get project")
		return nil
	}

	mergeRequests, err := p.getProjectMergeRequests(pid)
	if err != nil {
		log.WithError(err).Error("Failed to get project merge requests")
		return nil
	}

	log.Debugf("Merge requests found: %d", len(mergeRequests))

	var items []MessageItem
	for _, mergeRequest := range mergeRequests {
		if !p.config.WIP && mergeRequest.WorkInProgress {
			continue
		}

		labels := p.filterLabels(mergeRequest.Labels)
		reviewers := p.getMergeRequestReviewers(mergeRequest)
		canBeMerged := mergeRequest.MergeStatus == "can_be_merged"
		item := MessageItem{
			CanBeMerged:          canBeMerged,
			ProjectURL:           project.WebURL,
			ProjectName:          project.Name,
			ProjectID:            project.ID,
			MergeRequestCreateAt: *mergeRequest.CreatedAt,
			MergeRequestID:       mergeRequest.IID,
			MergeRequestTitle:    mergeRequest.Title,
			MergeRequestURL:      mergeRequest.WebURL,
			Labels:               labels,
			Reviewers:            reviewers,
		}
		if mergeRequest.Author != nil {
			item.Author = fmt.Sprintf("@%s", mergeRequest.Author.Username)
		}
		if p.config.CanBeMerged || len(item.Reviewers) != 0 {
			items = append(items, item)
		}
	}

	return items
}

func (p *PullReminders) processGroup(gid int) []MessageItem {
	log := p.logger.WithField("group_id", strconv.Itoa(gid))

	projects, err := p.getGroupProjects(gid)
	if err != nil {
		log.WithError(err).Error("Failed to get group projects")
		return nil
	}

	log.Debugf("Projects found: %d", len(projects))

	mergeRequests, err := p.getGroupMergeRequests(gid)
	if err != nil {
		log.WithError(err).Error("Failed to get group merge requests")
		return nil
	}

	log.Debugf("Merge requests found: %d", len(mergeRequests))

	var items []MessageItem
	for _, mergeRequest := range mergeRequests {
		if !p.config.WIP && mergeRequest.WorkInProgress {
			continue
		}

		project, ok := projects[mergeRequest.ProjectID]
		if !ok {
			log.Warnf("Project %d form Merge Request %d not found", mergeRequest.ProjectID, mergeRequest.ID)
			continue
		}

		labels := p.filterLabels(mergeRequest.Labels)
		reviewers := p.getMergeRequestReviewers(mergeRequest)
		canBeMerged := mergeRequest.MergeStatus == "can_be_merged"
		item := MessageItem{
			CanBeMerged:          canBeMerged,
			ProjectURL:           project.WebURL,
			ProjectName:          project.Name,
			ProjectID:            project.ID,
			MergeRequestCreateAt: *mergeRequest.CreatedAt,
			MergeRequestID:       mergeRequest.IID,
			MergeRequestTitle:    mergeRequest.Title,
			MergeRequestURL:      mergeRequest.WebURL,
			Labels:               labels,
			Reviewers:            reviewers,
		}
		if mergeRequest.Author != nil {
			item.Author = fmt.Sprintf("@%s", mergeRequest.Author.Username)
		}
		if p.config.CanBeMerged || len(item.Reviewers) != 0 {
			items = append(items, item)
		}
	}

	return items
}

func (p *PullReminders) run() {
	var items []MessageItem

	p.logger.Info("Do the hustle")

	for _, gid := range p.config.GroupIDs {
		groupItems := p.processGroup(gid)
		items = append(items, groupItems...)
	}

	for _, pid := range p.config.ProjectIDs {
		projectItems := p.projectProject(pid)
		items = append(items, projectItems...)
	}

	if len(items) == 0 {
		if len(p.config.EmptyMessage) == 0 {
			return
		}
		err := p.notifyText(p.config.EmptyMessage)
		if err != nil {
			p.logger.WithError(err).Error("Failed to notify")
		}
		return
	}

	err := p.notify(items)
	if err != nil {
		p.logger.WithError(err).Error("Failed to notify")
	}
}

type SlackRequest struct {
	Channel     string            `json:"channel,omitempty"`
	Username    string            `json:"username,omitempty"`
	IconEmoji   string            `json:"icon_emoji,omitempty"`
	IconURL     string            `json:"icon_url,omitempty"`
	LinkNames   bool              `json:"link_names,omitempty"`
	Attachments []SlackAttachment `json:"attachments"`
}

type SlackAttachment struct {
	Text     string   `json:"text"`
	MrkdwnIn []string `json:"mrkdwn_in,omitempty"`
}

func (p *PullReminders) notifyTelegram(text string) error {
	p.logger.Debug("Sends notification to Telegram")

	if p.config.DryRun {
		p.logger.Debugf("Notification text: %s", text)
		return nil
	}

	tgMessage := tgbotapi.NewMessage(p.config.TelegramConfig.ChatID, text)
	tgMessage.DisableWebPagePreview = true
	tgMessage.ParseMode = tgbotapi.ModeHTML
	_, err := p.telegramCli.Send(tgMessage)
	return err
}

func (p *PullReminders) notifySlack(text string) error {
	p.logger.Debug("Sends notification to Slack")

	if p.config.DryRun {
		p.logger.Debugf("Notification text: %s", text)
		return nil
	}

	req := &SlackRequest{
		Channel:   p.config.SlackConfig.Channel,
		IconEmoji: p.config.SlackConfig.IconEmoji,
		IconURL:   p.config.SlackConfig.IconURL,
		LinkNames: *p.config.SlackConfig.LinkNames,
		Username:  "Pull Reminders",
		Attachments: []SlackAttachment{
			{
				Text:     text,
				MrkdwnIn: []string{"fallback", "pretext", "text"},
			},
		},
	}
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req); err != nil {
		return err
	}
	resp, err := httpClient.Post(p.config.SlackConfig.APIURL, "application/json", &buf)
	if err != nil {
		return err
	}
	return resp.Body.Close()
}

func (p *PullReminders) notifyText(text string) error {
	if p.config.TelegramConfig.Enabled {
		err := p.notifyTelegram(text)
		if err != nil {
			return err
		}
	}
	if p.config.SlackConfig.Enabled {
		err := p.notifySlack(text)
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *PullReminders) notify(items []MessageItem) error {
	if p.config.TelegramConfig.Enabled {
		text := ""
		for _, item := range items {
			text += item.AsHTML() + "\n\n"
		}
		err := p.notifyTelegram(text)
		if err != nil {
			return err
		}
	}
	if p.config.SlackConfig.Enabled {
		text := ""
		for _, item := range items {
			text += item.AsMrkdwn() + "\n\n"
		}
		err := p.notifySlack(text)
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *PullReminders) printsCronNext() {
	for _, entry := range p.cronCli.Entries() {
		p.logger.Infof("Next run: %s", entry.Next.String())
	}
}
