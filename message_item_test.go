package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestMessageItem_AsHTML(t *testing.T) {
	tests := []struct {
		mi  MessageItem
		out string
	}{
		{
			mi: MessageItem{
				ProjectURL:           "http://gitlab.com",
				ProjectName:          "Google",
				MergeRequestURL:      "http://gitlab.com/mr/1",
				MergeRequestTitle:    "Merge Request 1",
				MergeRequestCreateAt: time.Now(),
				Reviewers:            []string{"@foobar"},
			},
			out: `[<a href="http://gitlab.com">Google</a>] <a href="http://gitlab.com/mr/1">Merge Request 1</a>
now · Waiting on @foobar`,
		},
		{
			mi: MessageItem{
				ProjectURL:           "http://gitlab.com",
				ProjectName:          "Google",
				MergeRequestURL:      "http://gitlab.com/mr/1",
				MergeRequestTitle:    "Merge Request 1",
				MergeRequestCreateAt: time.Now().Add(-5 * time.Minute),
				Labels:               []string{"label"},
				Reviewers:            []string{"@foobar"},
			},
			out: `[<a href="http://gitlab.com">Google</a>] <a href="http://gitlab.com/mr/1">Merge Request 1</a> (label)
5 minutes ago · Waiting on @foobar`,
		},
		{
			mi: MessageItem{
				CanBeMerged:          true,
				ProjectURL:           "http://gitlab.com",
				ProjectName:          "Google",
				MergeRequestURL:      "http://gitlab.com/mr/1",
				MergeRequestTitle:    "Merge Request 1",
				MergeRequestCreateAt: time.Now().Add(-5 * time.Minute),
				Labels:               []string{"label"},
				Reviewers:            nil,
			},
			out: `[<a href="http://gitlab.com">Google</a>] <a href="http://gitlab.com/mr/1">Merge Request 1</a> (label)
5 minutes ago · Can be merged`,
		},
		{
			mi: MessageItem{
				CanBeMerged:          true,
				ProjectURL:           "http://gitlab.com",
				ProjectName:          "Google",
				MergeRequestURL:      "http://gitlab.com/mr/1",
				MergeRequestTitle:    "Merge Request 1",
				MergeRequestCreateAt: time.Now().Add(-5 * time.Minute),
				Labels:               []string{"label"},
				Reviewers:            nil,
				Author:               "@leominov",
			},
			out: `[<a href="http://gitlab.com">Google</a>] <a href="http://gitlab.com/mr/1">Merge Request 1</a> (label)
5 minutes ago · Can be merged by @leominov`,
		},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, test.mi.AsHTML())
	}
}

func TestMessageItem_AsMrkdwn(t *testing.T) {
	tests := []struct {
		mi  MessageItem
		out string
	}{
		{
			mi: MessageItem{
				ProjectURL:           "http://gitlab.com",
				ProjectName:          "Google",
				MergeRequestURL:      "http://gitlab.com/mr/1",
				MergeRequestTitle:    "Merge Request 1",
				MergeRequestCreateAt: time.Now(),
				Reviewers:            []string{"@foobar"},
			},
			out: `[<http://gitlab.com|Google>] <http://gitlab.com/mr/1|Merge Request 1>
now · Waiting on @foobar`,
		},
		{
			mi: MessageItem{
				ProjectURL:           "http://gitlab.com",
				ProjectName:          "Google",
				MergeRequestURL:      "http://gitlab.com/mr/1",
				MergeRequestTitle:    "Merge Request 1",
				MergeRequestCreateAt: time.Now().Add(-5 * time.Minute),
				Labels:               []string{"label"},
				Reviewers:            []string{"@foobar"},
			},
			out: `[<http://gitlab.com|Google>] <http://gitlab.com/mr/1|Merge Request 1> (label)
5 minutes ago · Waiting on @foobar`,
		},
		{
			mi: MessageItem{
				CanBeMerged:          true,
				ProjectURL:           "http://gitlab.com",
				ProjectName:          "Google",
				MergeRequestURL:      "http://gitlab.com/mr/1",
				MergeRequestTitle:    "Merge Request 1",
				MergeRequestCreateAt: time.Now().Add(-5 * time.Minute),
				Labels:               []string{"label"},
				Reviewers:            nil,
			},
			out: `[<http://gitlab.com|Google>] <http://gitlab.com/mr/1|Merge Request 1> (label)
5 minutes ago · Can be merged`,
		},
		{
			mi: MessageItem{
				CanBeMerged:          true,
				ProjectURL:           "http://gitlab.com",
				ProjectName:          "Google",
				MergeRequestURL:      "http://gitlab.com/mr/1",
				MergeRequestTitle:    "Merge Request 1",
				MergeRequestCreateAt: time.Now().Add(-5 * time.Minute),
				Labels:               []string{"label"},
				Reviewers:            nil,
				Author:               "@leominov",
			},
			out: `[<http://gitlab.com|Google>] <http://gitlab.com/mr/1|Merge Request 1> (label)
5 minutes ago · Can be merged by @leominov`,
		},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, test.mi.AsMrkdwn())
	}
}
