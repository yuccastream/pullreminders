package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadConfig(t *testing.T) {
	_, err := LoadConfig("test_data/config-not-found.yaml")
	assert.Error(t, err)

	_, err = LoadConfig("test_data/config-invalid-syntax.yaml")
	assert.Error(t, err)

	_, err = LoadConfig("test_data/config-invalid-labels-match-re.yaml")
	assert.Error(t, err)

	config, err := LoadConfig("test_data/config-valid.yaml")
	if assert.NoError(t, err) {
		assert.True(t, *config.SlackConfig.LinkNames)
	}

	config, err = LoadConfig("test_data/config-valid-slack.yaml")
	if assert.NoError(t, err) {
		assert.False(t, *config.SlackConfig.LinkNames)
	}
}

func TestConfig_FillDefaults(t *testing.T) {
	c := &Config{}
	assert.Empty(t, c.BaseURL)
	assert.Empty(t, c.Method)
	assert.Nil(t, c.SlackConfig.LinkNames)

	c.FillDefaults()
	assert.Equal(t, defaultBaseURL, c.BaseURL)
	assert.Equal(t, defaultMethod, c.Method)
	assert.Equal(t, true, *c.SlackConfig.LinkNames)
}

func TestConfig_LoadSensitiveDataFromEnv(t *testing.T) {
	os.Unsetenv("ACCESS_TOKEN")
	os.Unsetenv("TELEGRAM_TOKEN")
	os.Unsetenv("SLACK_API_URL")

	c := &Config{}
	assert.Empty(t, c.AccessToken)
	assert.Empty(t, c.TelegramConfig.Token)
	assert.Empty(t, c.SlackConfig.APIURL)

	c.LoadSensitiveDataFromEnv()
	assert.Empty(t, c.AccessToken)
	assert.Empty(t, c.TelegramConfig.Token)
	assert.Empty(t, c.SlackConfig.APIURL)

	os.Setenv("ACCESS_TOKEN", "A")
	os.Setenv("TELEGRAM_TOKEN", "B")
	os.Setenv("SLACK_API_URL", "C")
	c.LoadSensitiveDataFromEnv()
	assert.Equal(t, c.AccessToken, "A")
	assert.Equal(t, c.TelegramConfig.Token, "B")
	assert.Equal(t, c.SlackConfig.APIURL, "C")
}
