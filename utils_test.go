package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnique(t *testing.T) {
	tests := []struct {
		in, out []string
	}{
		{
			in:  []string{},
			out: []string{},
		},
		{
			in:  []string{"foo"},
			out: []string{"foo"},
		},
		{
			in:  []string{"foo", "foo"},
			out: []string{"foo"},
		},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, Unique(test.in))
	}
}

func TestNewLogger(t *testing.T) {
	os.Unsetenv("LOG_LEVEL")
	os.Unsetenv("LOG_FORMAT")
	logger1 := NewLogger(os.Stdout, "test1")
	if assert.NotNil(t, logger1) {
		assert.Equal(t, "test1", logger1.Data["component"])
	}

	os.Setenv("LOG_LEVEL", "debug")
	os.Setenv("LOG_FORMAT", "text")
	logger2 := NewLogger(os.Stdout, "test2")
	if assert.NotNil(t, logger2) {
		assert.Equal(t, "test2", logger2.Data["component"])
	}
}
