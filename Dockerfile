FROM golang:1.16-alpine3.13 as builder
WORKDIR /go/src/gitlab.com/yuccastream/pullreminders
COPY . .
RUN go build -o pullreminders ./

FROM alpine:3.13
COPY --from=builder /go/src/gitlab.com/yuccastream/pullreminders/pullreminders /usr/local/bin/pullreminders
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENTRYPOINT ["pullreminders"]
