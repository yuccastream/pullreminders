package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
)

func TestReviewerNamesSuggestedByApprovals(t *testing.T) {
	tests := []struct {
		approvals *gitlab.MergeRequestApprovals
		result    []string
	}{
		{
			approvals: &gitlab.MergeRequestApprovals{
				SuggestedApprovers: []*gitlab.BasicUser{
					{
						Username: "foo",
					},
				},
			},
			result: []string{"foo"},
		},
		{
			approvals: &gitlab.MergeRequestApprovals{
				SuggestedApprovers: []*gitlab.BasicUser{
					{
						Username: "foo",
					},
					{
						Username: "foo",
					},
				},
			},
			result: []string{"foo"},
		},
		{
			approvals: &gitlab.MergeRequestApprovals{
				SuggestedApprovers: []*gitlab.BasicUser{
					{
						Username: "foo",
					},
				},
				ApprovedBy: []*gitlab.MergeRequestApproverUser{
					{
						User: &gitlab.BasicUser{
							Username: "foo",
						},
					},
				},
			},
			result: []string{},
		},
		{
			approvals: &gitlab.MergeRequestApprovals{
				SuggestedApprovers: []*gitlab.BasicUser{},
			},
			result: []string{},
		},
		{
			approvals: &gitlab.MergeRequestApprovals{
				SuggestedApprovers: nil,
			},
			result: []string{},
		},
	}
	for _, test := range tests {
		res := ReviewerNamesSuggestedByApprovals(test.approvals)
		assert.Equal(t, test.result, res)
	}
}

func TestReviewerNamesDirectByApprovals(t *testing.T) {
	tests := []struct {
		approvals *gitlab.MergeRequestApprovals
		result    []string
	}{
		{
			approvals: &gitlab.MergeRequestApprovals{
				Approvers: []*gitlab.MergeRequestApproverUser{
					{
						User: &gitlab.BasicUser{
							Username: "foo",
						},
					},
				},
			},
			result: []string{"foo"},
		},
		{
			approvals: &gitlab.MergeRequestApprovals{
				Approvers: []*gitlab.MergeRequestApproverUser{
					{
						User: &gitlab.BasicUser{
							Username: "foo",
						},
					},
					{
						User: &gitlab.BasicUser{
							Username: "foo",
						},
					},
				},
			},
			result: []string{"foo"},
		},
		{
			approvals: &gitlab.MergeRequestApprovals{
				Approvers: []*gitlab.MergeRequestApproverUser{
					{
						User: &gitlab.BasicUser{
							Username: "foo",
						},
					},
				},
				ApprovedBy: []*gitlab.MergeRequestApproverUser{
					{
						User: &gitlab.BasicUser{
							Username: "foo",
						},
					},
				},
			},
			result: []string{},
		},
		{
			approvals: &gitlab.MergeRequestApprovals{
				Approvers: []*gitlab.MergeRequestApproverUser{},
			},
			result: []string{},
		},
		{
			approvals: &gitlab.MergeRequestApprovals{
				Approvers: nil,
			},
			result: []string{},
		},
	}
	for _, test := range tests {
		res := ReviewerNamesDirectByApprovals(test.approvals)
		assert.Equal(t, test.result, res)
	}
}

func TestReviewerNamesByApprovals(t *testing.T) {
	tests := []struct {
		approvals *gitlab.MergeRequestApprovals
		result    []string
	}{
		{
			approvals: &gitlab.MergeRequestApprovals{
				SuggestedApprovers: []*gitlab.BasicUser{
					{
						Username: "foo",
					},
				},
				Approvers: []*gitlab.MergeRequestApproverUser{
					{
						User: &gitlab.BasicUser{
							Username: "foo",
						},
					},
				},
			},
			result: []string{"foo"},
		},
		{
			approvals: &gitlab.MergeRequestApprovals{
				SuggestedApprovers: []*gitlab.BasicUser{
					{
						Username: "foo",
					},
				},
				Approvers: nil,
			},
			result: []string{"foo"},
		},
		{
			approvals: &gitlab.MergeRequestApprovals{
				SuggestedApprovers: nil,
				Approvers:          nil,
			},
			result: []string{},
		},
	}
	for _, test := range tests {
		res := ReviewerNamesByApprovals(test.approvals)
		assert.Equal(t, test.result, res)
	}
}
